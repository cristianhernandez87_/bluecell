/**
 * Styles
 */
import "../scss/index.scss";

/**
 * Modules
 */
import { silcCoreInit } from 'silc-core';
import { silcAccordionInit } from 'silc-accordion';
import { silcNavInit } from 'silc-nav';
import { silcOffcanvasInit } from 'silc-offcanvas';
import { menu } from '../components/sections/s-menu/s-menu';

/**
 * Init
 */
menu();
silcCoreInit();
silcAccordionInit();
silcNavInit();
silcOffcanvasInit();
