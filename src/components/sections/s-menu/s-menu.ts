export function menu() {
    if(document.getElementById("menu-open")!=null){
		document.getElementById("menu-open").addEventListener("click", function() {
	        moveMenu();
	    });    
    }

    if(document.getElementById("btn-login")!=null){
		document.getElementById("btn-login").addEventListener("click", function() {
	        moveMenu();
	    });    
    }
    
    
    function moveMenu() {
        if (document.body.classList.contains('open-menu')) {
            document.body.classList.remove('open-menu');
        } else {
            document.body.classList.add('open-menu');
        };
    }
}