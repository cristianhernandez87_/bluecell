/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 10);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.__esModule = true;
var SilcAccordion_1 = __webpack_require__(6);
exports.SilcAccordion = SilcAccordion_1["default"];
function silcAccordionInit() {
    var accordions = document.querySelectorAll('.silc-accordion');
    if (accordions.length > 0) {
        for (var i = 0; i < accordions.length; i++) {
            new SilcAccordion_1["default"](accordions[i]);
        }
    }
}
exports.silcAccordionInit = silcAccordionInit;


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.__esModule = true;
var SilcCore_1 = __webpack_require__(7);
exports.SilcCore = SilcCore_1["default"];
function silcCoreInit() {
    new SilcCore_1["default"]();
}
exports.silcCoreInit = silcCoreInit;


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.__esModule = true;
var SilcNav_1 = __webpack_require__(8);
exports.SilcNav = SilcNav_1["default"];
function silcNavInit() {
    [].forEach.call(document.querySelectorAll('.silc-nav'), function (el) {
        new SilcNav_1["default"](el);
    });
}
exports.silcNavInit = silcNavInit;


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.__esModule = true;
var SilcOffcanvas_1 = __webpack_require__(9);
exports.SilcOffcanvas = SilcOffcanvas_1["default"];
function silcOffcanvasInit() {
    [].forEach.call(document.querySelectorAll('.silc-offcanvas__trigger'), function (el) {
        new SilcOffcanvas_1["default"](el);
    });
}
exports.silcOffcanvasInit = silcOffcanvasInit;


/***/ }),
/* 5 */
/***/ (function(module, exports) {

// element-closest | CC0-1.0 | github.com/jonathantneal/closest

(function (ElementProto) {
	if (typeof ElementProto.matches !== 'function') {
		ElementProto.matches = ElementProto.msMatchesSelector || ElementProto.mozMatchesSelector || ElementProto.webkitMatchesSelector || function matches(selector) {
			var element = this;
			var elements = (element.document || element.ownerDocument).querySelectorAll(selector);
			var index = 0;

			while (elements[index] && elements[index] !== element) {
				++index;
			}

			return Boolean(elements[index]);
		};
	}

	if (typeof ElementProto.closest !== 'function') {
		ElementProto.closest = function closest(selector) {
			var element = this;

			while (element && element.nodeType === 1) {
				if (element.matches(selector)) {
					return element;
				}

				element = element.parentNode;
			}

			return null;
		};
	}
})(window.Element.prototype);


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.__esModule = true;
// .closest() pollyfill
var elementClosest = __webpack_require__(5);
elementClosest;
var default_1 = /** @class */ (function () {
    /**
     * Constructor
     * @param {HTMLElement} element
     */
    function default_1(element) {
        // Set class properties
        this.element = element;
        this.labels = this.element.querySelectorAll('.silc-accordion__label');
        this.nav = this.element.querySelector('.silc-accordion__nav-items');
        this.settings = this.applySettings();
        // Label event listener
        if (this.labels.length) {
            this.labelEventListener();
        }
        // Nav event listener
        if (this.settings.tabs && this.nav !== undefined) {
            this.navEventListener();
            this.nav.querySelector('.silc-accordion__nav-link').classList.add('silc-accordion__nav-link--active');
            this.element.querySelector('.silc-accordion__content').classList.add('silc-accordion__content--visible-persist');
        }
        // Open first element
        if (this.settings.openFirst) {
            this.element.querySelector('.silc-accordion__label').classList.add('silc-accordion__label--active');
            this.element.querySelector('.silc-accordion__content').classList.add('silc-accordion__content--visible');
        }
    }
    /**
     * Apply accordion settings
     */
    default_1.prototype.applySettings = function () {
        // Defaults
        var settings = {
            tabs: false,
            openMultiple: false,
            openFirst: false
        };
        if (this.element.classList.contains('silc-accordion--become-tabs') ||
            this.element.classList.contains('silc-accordion--tabs')) {
            settings.tabs = true;
        }
        if (this.element.getAttribute('data-silc-accordion-open-multiple') !== null) {
            settings.openMultiple = true;
        }
        if (this.element.getAttribute('data-silc-accordion-open-first') !== null) {
            settings.openFirst = true;
        }
        return settings;
    };
    /**
     * Event listener for accordion labels
     */
    default_1.prototype.labelEventListener = function () {
        var _this = this;
        this.element.addEventListener('click', function (event) {
            // Get target from event
            var target = event.target;
            // If target contains label class
            if (target.classList.contains('silc-accordion__label')) {
                event.preventDefault();
                // Get clicked labels associated content element
                var content = _this.getContent(target);
                // Toggle the content
                _this.toggleContent(content);
                // Toggle active element
                _this.toggleActiveLabel(target, 'silc-accordion__label--active');
            }
            event.stopPropagation();
        });
    };
    /**
     * Event listener for tabs navigation
     */
    default_1.prototype.navEventListener = function () {
        var _this = this;
        this.nav.addEventListener('click', function (event) {
            var target = event.target;
            if (target.classList.contains('silc-accordion__nav-link')) {
                event.preventDefault();
                _this.toggleTab(target);
            }
            event.stopPropagation();
        });
    };
    /**
     * Gets content element from clicked label
     * @param {Element} label
     */
    default_1.prototype.getContent = function (label) {
        return label.parentNode.nextElementSibling;
    };
    /**
     * Gets accordion based on id
     * @param {String} id - id of content to get
     */
    default_1.prototype.getById = function (id) {
        return {
            'content': this.element.querySelector(id + ' .silc-accordion__content'),
            'label': this.element.querySelector(id + ' .silc-accordion__label')
        };
    };
    /**
     * Toggle tab from clicked nav link
     * @param {Element} link - link element clicked
     */
    default_1.prototype.toggleTab = function (link) {
        var targetId = link.getAttribute('href');
        var accordion = this.getById(targetId);
        this.hideAllPersitentVisible();
        this.toggleContent(accordion.content);
        this.toggleActiveTab(link, 'silc-accordion__nav-link--active');
        this.toggleActiveLabel(accordion.label, 'silc-accordion__label--active');
        // Ensure that one tab is always open
        accordion.content.classList.add('silc-accordion__content--visible-persist');
    };
    /**
     * Show content
     * @param {Element} el
     */
    default_1.prototype.toggleContent = function (el) {
        if (!this.settings.openMultiple) {
            this.removeCssClass('silc-accordion__content--visible', el);
        }
        el.classList.toggle('silc-accordion__content--visible');
    };
    /**
     * Hide all persistent visible content
     * Persistent visible class is used for accordions that transform to tabs
     */
    default_1.prototype.hideAllPersitentVisible = function () {
        this.removeCssClass('silc-accordion__content--visible-persist');
    };
    /**
     * Remove CSS class from all matching elements
     * @param className
     */
    default_1.prototype.removeCssClass = function (className, excludeEl) {
        // Hide all persitent visible content
        var children = this.element.querySelectorAll('.' + className);
        if (children.length > 0) {
            for (var i = 0; i < children.length; i++) {
                var el = children[i];
                if (el !== excludeEl && this.element === el.closest('.silc-accordion')) {
                    el.classList.remove(className);
                }
            }
        }
    };
    /**
     * Set active label
     * @param el
     * @param className
     */
    default_1.prototype.toggleActiveLabel = function (el, className) {
        if (!this.settings.openMultiple) {
            var currentActive = this.element.querySelector('.' + className);
            if (currentActive && currentActive !== el && this.element === currentActive.closest('.silc-accordion')) {
                currentActive.classList.remove(className);
            }
        }
        el.classList.toggle(className);
    };
    /**
     * Set active tab
     * @param el
     * @param className
     */
    default_1.prototype.toggleActiveTab = function (el, className) {
        // Get current active tab
        var currentActive = this.element.querySelector('.' + className);
        // Remove active class
        currentActive.classList.remove(className);
        // Add active class to clicked tab
        el.classList.add(className);
    };
    return default_1;
}());
exports["default"] = default_1;


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.__esModule = true;
var default_1 = (function () {
    function default_1() {
        document.body.classList.add('js');
    }
    return default_1;
}());
exports["default"] = default_1;


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.__esModule = true;
var default_1 = (function () {
    /**
     * Contructor
     * @param element
     */
    function default_1(element) {
        var _this = this;
        this.moveClass = 'silc-nav__move';
        this.itemsClass = 'silc-nav__items';
        this.itemClass = 'silc-nav__item';
        this.linkClass = 'silc-nav__link';
        // Save shortcut to element
        this.element = element;
        // Add BEM classes
        this.addBemClasses();
        // Save shortcut to root items
        this.rootItems = element.querySelector('.' + this.itemsClass);
        // Set initial position
        this.position = 0;
        // Create move controle
        this.createMoveControls();
        // Attach click listener
        this.element.addEventListener('click', function (event) {
            _this.moveListener(event);
        });
    }
    /**
     * Add BEM classes
     */
    default_1.prototype.addBemClasses = function () {
        var _this = this;
        [].forEach.call(this.element.querySelectorAll('ul'), function (el) {
            el.classList.add(_this.itemsClass);
        });
        [].forEach.call(this.element.querySelectorAll('li'), function (el) {
            el.classList.add(_this.itemClass);
        });
        [].forEach.call(this.element.querySelectorAll('a'), function (el) {
            el.classList.add(_this.linkClass);
        });
    };
    /**
     * Create controls for moving forward and backward
     */
    default_1.prototype.createMoveControls = function () {
        var _this = this;
        // For each nav items
        [].forEach.call(this.rootItems.querySelectorAll('.' + this.itemsClass), function (items) {
            // Get elements
            var item = items.parentNode;
            var link = item.querySelector('.' + _this.linkClass);
            var childItems = item.querySelector('.' + _this.itemsClass);
            var childItemsFirstItem = childItems.querySelector('.' + _this.itemClass);
            // Get link text
            var linkText = link.innerText;
            // Add parent class
            item.classList.add(_this.itemClass + '--parent');
            // Create more element
            var forward = document.createElement('span');
            forward.classList.add(_this.moveClass, _this.moveClass + '--forward');
            forward.innerHTML = 'More ' + linkText;
            // Create back element
            var back = document.createElement('li');
            back.classList.add(_this.itemClass, _this.moveClass, _this.moveClass + '--back');
            back.innerHTML = linkText;
            // Add forward and back link
            link.appendChild(forward);
            childItems.insertBefore(back, childItemsFirstItem);
        });
    };
    /**
     * Listen for clicks on move elements
     * @param event
     */
    default_1.prototype.moveListener = function (event) {
        var target = event.target;
        if (target.classList.contains(this.moveClass + '--forward') || target.classList.contains(this.moveClass + '--back')) {
            event.preventDefault();
            if (target.classList.contains(this.moveClass + '--forward')) {
                this.position++;
                this.move(target, 'forward');
            }
            else {
                this.position--;
                this.move(target, 'back');
            }
        }
        event.stopPropagation();
    };
    /**
     * Move through navigation when collapsed
     * @param target
     */
    default_1.prototype.move = function (target, direction) {
        var _this = this;
        // Get parent item
        var parentItem = target.parentNode.parentNode;
        // Hide everything
        [].forEach.call(this.rootItems.querySelectorAll('.' + this.itemsClass), function (el) {
            el.classList.add(_this.itemsClass + '--hidden');
        });
        // Show selected branch
        [].forEach.call(parentItem.querySelectorAll('.' + this.itemsClass), function (el) {
            el.classList.remove(_this.itemsClass + '--hidden');
        });
        // Show selected branch parent tree
        [].forEach.call(this.getParents(target, '.' + this.itemsClass), function (el) {
            el.classList.remove(_this.itemsClass + '--hidden');
        });
        // Get parent items
        var parentItems = direction === 'forward'
            ? parentItem.querySelector('.' + this.itemsClass)
            : parentItem.parentNode;
        // Add CSS for move
        this.rootItems.style.left = (this.position * -100) + '%';
        // Add CSS for height
        this.rootItems.style.height = this.position > 0
            ? parentItems.offsetHeight + 'px'
            : 'auto';
    };
    /**
     * Get parent elements of passed in selector
     * @param elem
     * @param selector
     */
    default_1.prototype.getParents = function (elem, selector) {
        // Element.matches() polyfill for IE and Safari
        if (!Element.prototype.matches) {
            Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;
        }
        // Setup parents array
        var parents = [];
        // Get matching parent elements
        for (; elem && elem !== document; elem = elem.parentNode) {
            // Add matching parents to array
            if (selector && elem.matches(selector)) {
                parents.push(elem);
            }
        }
        return parents;
    };
    return default_1;
}());
exports["default"] = default_1;


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.__esModule = true;
var default_1 = (function () {
    /**
     * Constructor
     * @param element
     */
    function default_1(element) {
        var _this = this;
        // Shortcut to trigger
        this.trigger = element;
        // Get target element
        this.target = this.getTarget(this.trigger);
        // Event listener for trigger
        if (this.target !== undefined) {
            this.trigger.addEventListener('click', function (e) {
                _this.toggle(e);
            });
        }
    }
    /**
     * Get target element for trigger
     * @param trigger
     */
    default_1.prototype.getTarget = function (trigger) {
        if (!trigger.hasAttribute('href')) {
            console.log('silc offcanvas: Trigger is missing href attribute');
            return undefined;
        }
        var targetSelector = trigger.getAttribute('href');
        var targetEl = document.querySelector(targetSelector);
        if (targetEl === undefined) {
            console.log('silc offcanvas: Target element ' + targetSelector + ' does not exist');
        }
        return targetEl;
    };
    /**
     * Stop target iframe videos from playing
     * @param target
     */
    default_1.prototype.stopIframeVideo = function (target) {
        [].forEach.call(target.querySelectorAll('iframe'), function (el) {
            var src = el.getAttribute('src');
            el.setAttribute('src', src);
        });
    };
    /**
     * Toggle visibility of the target
     * @param event
     */
    default_1.prototype.toggle = function (event) {
        event.preventDefault();
        if (this.target.classList.contains('silc-offcanvas--visible')) {
            this.stopIframeVideo(this.target);
        }
        this.target.classList.toggle('silc-offcanvas--visible');
    };
    return default_1;
}());
exports["default"] = default_1;


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Styles
 */
__webpack_require__(0);
/**
 * Modules
 */
var silc_core_1 = __webpack_require__(2);
var silc_accordion_1 = __webpack_require__(1);
var silc_nav_1 = __webpack_require__(3);
var silc_offcanvas_1 = __webpack_require__(4);
var s_menu_1 = __webpack_require__(15);
/**
 * Init
 */
s_menu_1.menu();
silc_core_1.silcCoreInit();
silc_accordion_1.silcAccordionInit();
silc_nav_1.silcNavInit();
silc_offcanvas_1.silcOffcanvasInit();


/***/ }),
/* 11 */,
/* 12 */,
/* 13 */,
/* 14 */,
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
function menu() {
    if (document.getElementById("menu-open") != null) {
        document.getElementById("menu-open").addEventListener("click", function () {
            moveMenu();
        });
    }
    if (document.getElementById("btn-login") != null) {
        document.getElementById("btn-login").addEventListener("click", function () {
            moveMenu();
        });
    }
    function moveMenu() {
        if (document.body.classList.contains('open-menu')) {
            document.body.classList.remove('open-menu');
        }
        else {
            document.body.classList.add('open-menu');
        }
        ;
    }
}
exports.menu = menu;


/***/ })
/******/ ]);